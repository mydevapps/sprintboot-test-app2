docker stop test-app
docker rm test-app
docker rmi test-app
mvn package
docker build -t test-app .
docker save test-app > test-app.tar
scp test-app.tar vagrant@172.28.128.3:/home/vagrant
